//
//  WebViewController.swift
//  myVem
//
//  Created by Federico Foschini on 22/03/17.
//  Copyright © 2017 myDev S.r.l. All rights reserved.
//

import Foundation
import UIKit
import SafariServices
import WebKit
import CoreData
import CoreBluetooth
import Firebase

import UserNotifications

import WebViewJavascriptBridge
import WebKitPlus

class WebViewController: UIViewController, WKNavigationDelegate, WKUIDelegate, UNUserNotificationCenterDelegate, UIScrollViewDelegate, JavaScriptBridgeProtocol {
    
    
    @IBOutlet var containerView : UIView! = nil
    let coreDataManager = CoreDataManager()
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    weak var webView: WKWebView?
    var jsBridge: JavaScriptBridge!
    
    var logoutErrorDone = false
    var urlString: NSURL = Configurations.WebAppUrl!
    var userName = ""
    var userPassword = ""
    var loginError = 0
    var cookies: String = ""
    var progressView: UIProgressView!
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        print("Error: \(error.localizedDescription)")
        
    }
    func canRotate() -> Void {}
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        if (self.isMovingFromParentViewController) {
            UIDevice.current.setValue(Int(UIInterfaceOrientation.portrait.rawValue), forKey: "orientation")
        }
        // Show the navigation bar on other view controllers
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    //MARK: UIScrollView Delegate Methods
    func scrollViewWillBeginZooming(_ scrollView: UIScrollView, with view: UIView?){
        scrollView.pinchGestureRecognizer?.isEnabled = false
       // scrollView.panGestureRecognizer.isEnabled = false
        //For Avoid Zoom in Webview's content we are disable pinchGestureRecognizer
    }

    
    func webView(webView: WKWebView, willSendRequestForAuthenticationChallenge challenge:
        URLAuthenticationChallenge, completionHandler: (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
        print("didReceiveAuthenticationChallenge called")
        
        if challenge.protectionSpace.host == Configurations.WebAppDomain {
            loginError = loginError + 1
            //let cred = URLCredential(trust: challenge.protectionSpace.serverTrust!)
            //completionHandler(URLSession.AuthChallengeDisposition.useCredential, cred)
            
            let credential = URLCredential(user: userName, password: userPassword, persistence: URLCredential.Persistence.forSession)
            //challenge.sender?.use(credential, for: challenge)
            completionHandler(URLSession.AuthChallengeDisposition.useCredential, credential)
        } else {
            let credentials = URLCredential(trust: challenge.protectionSpace.serverTrust!)
            completionHandler(URLSession.AuthChallengeDisposition.useCredential, credentials)
        }
    }
    
    func webView(_ webView: WKWebView, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
        
        let webAppDomainHost = challenge.protectionSpace.host == Configurations.WebAppDomain
                                || challenge.protectionSpace.host == Configurations.WebAppDomainTest
        
        // if challenge already required, return to login
        if(!logoutErrorDone && webAppDomainHost && loginError >= 3) {
            logoutErrorDone = true
            
            // error message
            UIAlertView(
                title: "Error".localized,
                message: "LoginError".localized,
                delegate: self,
                cancelButtonTitle: "OK")
                .show()
            
            logOut()
        }
        
        print("didReceiveAuthenticationChallenge called")
        print("Challenge host: \(challenge.protectionSpace.host)")
        
        if webAppDomainHost {
            print("webAppDomainHost: true")
            loginError = loginError + 1
            //let cred = URLCredential(trust: challenge.protectionSpace.serverTrust!)
            //completionHandler(URLSession.AuthChallengeDisposition.useCredential, cred)
            
            let credential = URLCredential(user: userName, password: userPassword, persistence: URLCredential.Persistence.forSession)
            //challenge.sender?.use(credential, for: challenge)
            completionHandler(URLSession.AuthChallengeDisposition.useCredential, credential)
        } else {
            print("webAppDomainHost: false")
            let credentials = URLCredential(trust: challenge.protectionSpace.serverTrust!)
            completionHandler(URLSession.AuthChallengeDisposition.useCredential, credentials)
        }
    }
    
     func applicationWillEnterForeground(application: UIApplication)
    {
        print("app will enter foreground");
    }
    
     func applicationDidBecomeActive(application: UIApplication) {
        print("app did become active");
    }
    

    //// Spinner
    private var loadingObservation: NSKeyValueObservation?
    
    private lazy var loadingIndicator: UIActivityIndicatorView = {
        let spinner = UIActivityIndicatorView()
        spinner.translatesAutoresizingMaskIntoConstraints = false
        spinner.color = .black
        return spinner
    }()
    
    
    
    override func viewDidAppear(_ animated: Bool) {

        super.viewDidAppear(animated)
        
        // Abilito anche il landscape
        AppDelegate.AppUtility.lockOrientation(.all)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print("WebApp URL: \(Configurations.WebAppUrl?.absoluteString!)")
        print("WebApp Domain: \(Configurations.WebAppDomain)")
        print("Username: \(userName)")
        print("Password: \(userPassword)")
        print("Cookies: \(cookies)")
        
        loginError = 0
        logoutErrorDone = false
        coreDataManager.setAppDelegate(delegate: appDelegate)
        jsBridge = JavaScriptBridge()
        
        
        let webView = setupWebView()
        webView.autoresizingMask = UIViewAutoresizing.flexibleWidth; UIViewAutoresizing.flexibleHeight
        containerView.addSubview(webView)
        
        self.webView = webView
        //self.webView?.navigationDelegate = self
        
        self.webView?.navigationDelegate = self
        self.webView?.uiDelegate = self
        self.webView?.scrollView.delegate = self
        
        // setto la safe area, in questo modo correggo la banda bianca nella status bar di iphone x
        self.webView?.translatesAutoresizingMaskIntoConstraints = false
        if #available(iOS 11.0, *) {
            let guide = self.view.safeAreaLayoutGuide
            self.webView?.trailingAnchor.constraint(equalTo: guide.trailingAnchor).isActive = true
            self.webView?.leadingAnchor.constraint(equalTo: guide.leadingAnchor).isActive = true
            self.webView?.bottomAnchor.constraint(equalTo: guide.bottomAnchor).isActive = true
            self.webView?.topAnchor.constraint(equalTo: guide.topAnchor).isActive = true
        }
        //self.webView?.autoresizingMask = UIViewAutoresizing(rawValue: UIViewAutoresizing.RawValue(UInt8(UIViewAutoresizing.flexibleWidth.rawValue) | UInt8(UIViewAutoresizing.flexibleHeight.rawValue)))
        
        self.webView?.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        // set the target URL basing on the target news details ID (if set)
        // Se ho cliccato su una notifica apro la pagina degli eventi
        let targetEventsId = appDelegate.targetEventDetailsID
        if (targetEventsId != "") {
            let targetUrl = Configurations.UrlEvents?.absoluteString
            urlString = NSURL(string: targetUrl!)!
            appDelegate.targetEventDetailsID = ""
        } else {
            urlString = Configurations.UrlMap!
        }

    
        self.webView?.load(URLRequest(url:urlString as URL))
        
        // Aggiungo lo spinner nella navigazione, al cambio della pagina viene lanciato lo spinner
        loadingObservation = webView.observe(\.isLoading, options: [.new, .old]) { [weak self] (_, change) in
            guard let strongSelf = self else { return }
            
            // this is fine
            let new = change.newValue!
            let old = change.oldValue!
            
            if new && !old {
                strongSelf.view.addSubview(strongSelf.loadingIndicator)
                strongSelf.loadingIndicator.startAnimating()
                NSLayoutConstraint.activate([strongSelf.loadingIndicator.centerXAnchor.constraint(equalTo: strongSelf.view.centerXAnchor),
                                             strongSelf.loadingIndicator.centerYAnchor.constraint(equalTo: strongSelf.view.centerYAnchor)])
                strongSelf.view.bringSubview(toFront: strongSelf.loadingIndicator)
            }
            else if !new && old {
                strongSelf.loadingIndicator.stopAnimating()
                strongSelf.loadingIndicator.removeFromSuperview()
            }
        }
        
        jsBridge.setBridgeWebView(view: webView)
        jsBridge.setProtocol(targetProtocol: self)
        
        
        UNUserNotificationCenter.current().delegate = self as! UNUserNotificationCenterDelegate
    }
    

    // Inteecetto le chiamate nela sezione Call NOC/SOC
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        if navigationAction.request.url?.scheme == "tel" {
            UIApplication.shared.openURL(navigationAction.request.url!)
            decisionHandler(.cancel)
        } else {
            decisionHandler(.allow)
        }
    }
    
    /*if #available(iOS 10.0, *) {
        self.webView.configuration.dataDetectorTypes = .phoneNumber
    }
    */
    public func setupWebView() -> WKWebView{
        let userContentController = WKUserContentController()
        let webViewConfig = WKWebViewConfiguration()
        
        // Javascript that disables pinch-to-zoom by inserting the HTML viewport meta tag into <head>
        let source: String = "var meta = document.createElement('meta');" +
            "meta.name = 'viewport';" +
            "meta.content = 'width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no';" +
            "var head = document.getElementsByTagName('head')[0];" + "head.appendChild(meta);";
        let script: WKUserScript = WKUserScript(source: source, injectionTime: .atDocumentEnd, forMainFrameOnly: true)
        
       /* let preferences = WKPreferences()
        preferences.javaScriptEnabled = true
        preferences.javaScriptCanOpenWindowsAutomatically = true
        webViewConfiguration.preferences = preferences
        let contentController = WKUserContentController();
        contentController.add(
            jsBridge,
            name: "callbackHandler"
        )*/
        
        
        if let cookies = HTTPCookieStorage.shared.cookies {
            let script = getJSCookiesString(cookies: cookies)
            let cookieScript = WKUserScript(source: script, injectionTime: WKUserScriptInjectionTime.atDocumentStart, forMainFrameOnly: false)
            userContentController.addUserScript(cookieScript)
        }
        
        userContentController.add(
            jsBridge,
            name: "callbackHandler"
        )
        
        userContentController.addUserScript(script)
        
        webViewConfig.userContentController = userContentController
        webViewConfig.preferences.javaScriptCanOpenWindowsAutomatically = true
      //  let contentController = WKUserContentController();
        
        //webViewConfig.userContentController = contentController
        let frameWV = self.containerView.bounds
        let webViewFrame = CGRect(x: 0, y: 25, width: frameWV.size.width, height: frameWV.size.height - 25)
        
        return WKWebView(frame: webViewFrame, configuration: webViewConfig)
    }
    
    ///Generates script to create given cookies
    public func getJSCookiesString(cookies: [HTTPCookie]) -> String {
        var result = ""
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = NSTimeZone(abbreviation: "UTC") as! TimeZone
        dateFormatter.dateFormat = "EEE, d MMM yyyy HH:mm:ss zzz"
        
        for cookie in cookies {
            result += "document.cookie='\(cookie.name)=\(cookie.value); domain=\(cookie.domain); path=\(cookie.path); "
            if let date = cookie.expiresDate {
                result += "expires=\(dateFormatter.string(from: date)); "
            }
            if (cookie.isSecure) {
                result += "secure; "
            }
            result += "'; "
        }
        return result
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Hide the navigation bar on the this view controller
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        print("didReceiveMemoryWarning called")
    }
    
    override func dismiss(animated flag: Bool, completion: (() -> Void)?) {
        if (self.presentedViewController != nil) {
            super.dismiss(animated: flag, completion: completion)
        }
    }
    
    // Disable zooming in webView
    func viewForZooming(in: UIScrollView) -> UIView? {
        return nil;
    }
    
    // { JavaScriptBridgeProtocol implementation
    
    func openLogin() {
        self.performSegue(withIdentifier: "LogOutSegue", sender: self)
    }
    
    
    func logOut() {
        // reset application badge number
        UIApplication.shared.applicationIconBadgeNumber = 0
        
        print("reset user core data and logout")
        coreDataManager.resetUserData()
        print(coreDataManager.loadUserLogged())
        appDelegate.disconnectFromFcm()
        
        var cookieStorage: [HTTPCookie]? = HTTPCookieStorage.shared.cookies(for: URL(string: (Configurations.WebAppBaseUrl))!)
        cookieStorage?.removeAll()

        // Vuoto la cache
        WebCacheCleaner.clean()
        
        self.performSegue(withIdentifier: "LogOutSegue", sender: self)
    }
    
    func openLink(linkToOpen: String) {
        print("open link: [\(linkToOpen)]")
        //UIApplication.shared.openURL(URL(string: linkToOpen)!)
        
        // open the SafariViewController
        let safariVC = SFSafariViewController(url: URL(string: linkToOpen)! as URL)
        self.present(safariVC, animated: true, completion: nil)
    }
    
    func getFireBaseToken() -> String{
        print("get FireBase Token]")
        let tokenCur = InstanceID.instanceID().token()!
        print(tokenCur)
        return tokenCur
    }
    
    // }
    
    // { UNUserNotificationCenterDelegate implementation
    
    // Receive displayed notifications for iOS 10 devices.
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        if(coreDataManager.loadUserLogged() != nil) {
            // reset application badge number
            UIApplication.shared.applicationIconBadgeNumber = 0
            
            completionHandler(UNNotificationPresentationOptions.alert)
        }
        //completionHandler(UNNotificationPresentationOptions.alert)
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        
        if(coreDataManager.loadUserLogged() != nil) {
            // reset application badge number
            //UIApplication.shared.applicationIconBadgeNumber = 0
            
       
            
            print("Response Notification Action [\(response.actionIdentifier)]")
            print("Response Notification Request ID [\(response.notification.request.identifier)]")
            
            // Payload data
            let bodyData = response.notification.request.content.userInfo["body"] as! String
            let titleData = response.notification.request.content.userInfo["title"] as! String
            let idData = response.notification.request.content.userInfo["id"] as! String
            print("[Data] id:\(idData)")
            
            
            switch response.actionIdentifier {
            default:
                print("WebView - Default Action")
                let targetUrl = Configurations.UrlEvents?.absoluteString
                webView?.load(URLRequest(url: NSURL(string: targetUrl!)! as URL))
                jsBridge.setBridgeWebView(view: webView!)
                break
            }
            completionHandler()
        }
    }
    
    // setto il colore del testo nella status bar in bianco
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    
    // }
}

