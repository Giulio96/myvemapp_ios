//
//  Configurations.swift


//  myVem
//
//  Created by Federico Foschini on 23/03/17.
//  Copyright © 2017 myDev S.r.l. All rights reserved.
//

import Foundation

/* 
 * Main configurations of the App
 */
class Configurations {
    
    // MARK - COMMON SETTINGS
    
    //static let WebAppBaseUrl = "http://192.168.237.42/AppMyVem"
    static let Domain = "mvc.nms.vg";
    //static let Domain = "192.168.237.42";
    static let WebAppBaseUrl = "https://" + Configurations.Domain + "/AppMyVem"
    static let NotificationTitle = "myvem"
    
    static let WebAppDomain = Configurations.Domain + "/AppMyVem"
    static let WebAppDomainTest = Configurations.Domain + "/AppMyVem"

    // MARK - PRODUCTION SETTINGS
    static let WebAppUrl = NSURL(string: Configurations.WebAppBaseUrl)
    static let ProjectName = "myvem"
    static let UrlLogin = NSURL(string: Configurations.WebAppBaseUrl  + "/Account/LoginIOs")
    static let UrlMap = NSURL(string: Configurations.WebAppBaseUrl  + "/Map")
    static let UrlEvents = NSURL(string: Configurations.WebAppBaseUrl  + "/Events")
    static let UrlVerifyAccessDevice = NSURL(string: Configurations.WebAppBaseUrl  + "/api/VerifyAccessDevice")
    static let secretForAccessDevice = "asdeklfYTkOKllmLkmlIjpoKpokokPOUIjofkofjireokfKJHLJnnlJNSLkjnkjfgdfgdgyrettydfGDFGRsdfdfGF"
    static let UrlCheckSignUp = NSURL(string: Configurations.WebAppBaseUrl  + "/Account/CheckSignup")
    static let UrlSignUp = NSURL(string: Configurations.WebAppBaseUrl  + "/Account/Register")
    static let UrlPrivacy = NSURL(string: Configurations.WebAppBaseUrl  + "/PrivacyMyVem/")
    
}
