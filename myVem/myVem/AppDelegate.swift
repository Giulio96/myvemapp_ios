//
//  AppDelegate.swift
//  myVem
//
//  Created by Federico Foschini on 22/03/17.
//  Copyright © 2017 myDev S.r.l. All rights reserved.
//

import UIKit
import CoreData
import UserNotifications
import Firebase
import SwiftHTTP
import JSONJoy

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate {
    
    var window: UIWindow?
    var targetEventDetailsID = ""
    var showNotification = false
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        // reset application badge number
        UIApplication.shared.applicationIconBadgeNumber = 0
        
        //create the notificationCenter
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self as! UNUserNotificationCenterDelegate
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
            
            // For iOS 10 data message (sent via FCM)
            //FIRMessaging.messaging().remoteMessageDelegate = self
            
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        application.registerForRemoteNotifications()
        FirebaseApp.configure()
        return true
    }

    func application(application: UIApplication, didReceiveRemoteNotification userInfo: [NSObject : AnyObject], fetchCompletionHandler completionHandler: (UIBackgroundFetchResult) -> Void) {
        // handle your message
        completionHandler(UIBackgroundFetchResult.newData)
    }
    
    // [START refresh_token]
    func messaging(_ messaging: Messaging, didRefreshRegistrationToken fcmToken: String) {
        print("Firebase registration token: \(fcmToken)")

        // Connect to FCM since connection may have failed when attempted before having a token.
        connectToFcm(tokenHasBeenRefreshed: true)
    }
    
    func tokenRefreshNotification(_ notification: Notification) {
        if let refreshedToken = InstanceID.instanceID().token() {
            print("InstanceID token: \(refreshedToken)")
        }
        
        // Connect to FCM since connection may have failed when attempted before having a token.
        connectToFcm(tokenHasBeenRefreshed: true)
    }
    // [END refresh_token]
    
    // [START disconnect_from_fcm]
    func disconnectFromFcm() {
        // Disconnect previous FCM connection if it exists.
        Messaging.messaging().disconnect()
        print("Disconnecting from FCM...")
    }
    // [END disconnect_from_fcm]

    // [START connect_to_fcm]
    func connectToFcm(tokenHasBeenRefreshed: Bool) {
        print("Connecting to FCM...")
        // Won't connect since there is no token
        guard InstanceID.instanceID().token() != nil else {
            return
        }
        
        print("Notification token:  \(InstanceID.instanceID().token())")
        
        
        
        let coreDataManager = CoreDataManager()
        coreDataManager.setAppDelegate(delegate: self)
        if(coreDataManager.loadUserLogged() == nil){
            return
        }
        
        // delete the saved token if it has been refreshed
        if tokenHasBeenRefreshed {
            coreDataManager.resetNotificationTokenData()
        }
        
        // Disconnect previous FCM connection if it exists.
        Messaging.messaging().disconnect()
        
        Messaging.messaging().connect { (error) in
            if error != nil {
                print("Unable to connect with FCM. \(error?.localizedDescription ?? "")")
            } else {
                print("Connected to FCM.")
            }
        }
        
        if let refreshedToken = InstanceID.instanceID().token() {
            print("InstanceID token: \(refreshedToken)")
            
            /*
             * If the token has never been saved, the request will be done to the notification APIs
             * Otherwise, the token will be got from the core data
             */
            /*var doRequest: Bool = false
            let tokenData : NotificationToken? = coreDataManager.loadNotificationTokenData()
            doRequest = tokenData == nil
            if tokenData != nil {
                print("Token in Core Data: \(tokenData?.token ?? "-")")
                print("Request Result in Core Data: \(tokenData?.requestResult ?? false)")
                doRequest = tokenData?.requestResult == false
            }
            
            if doRequest {
                let params = ["ClientId" : refreshedToken]
                do {
                    let opt = try HTTP.POST(Configurations.NotificationApiUrlSubscribeString,
                                        parameters: params,
                                        headers: ["Content-Type": "application/json", "Token" : Configurations.ProjectName])
                    opt.start { response in
                        do {
                             print("[AppDelegate - Notification Hub Request] Response", response.data)
                            let resp = try NotificationServiceResponse(JSONLoader(response.data))
                            print("[AppDelegate - Notification Hub Request] Request finished: \(resp.success ?? false)")
                        
                            if let err = resp.error {
                                print("[AppDelegate - Notification Hub Request] Got an error: \(err)")
                            } else {
                                coreDataManager.saveNotificationTokenData(token: refreshedToken, requestResult: resp.success!)
                            }
                        } catch {
                            print("[AppDelegate - Notification Hub Request] Unable to parse the JSON")
                        }
                    }
                
                } catch let error {
                    print("[AppDelegate - Notification Hub Request] Got an error creating the request: \(error)")
                }
            }*/
        }
    }
    // [END connect_to_fcm]
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Unable to register for remote notifications: \(error.localizedDescription)")
    }
    
    // This function is added here only for debugging purposes, and can be removed if swizzling is enabled.
    // If swizzling is disabled then this function must be implemented so that the APNs token can be paired to
    // the InstanceID token.
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        var token = ""
        for i in 0..<deviceToken.count {
            token = token + String(format: "%02.2hhx", arguments: [deviceToken[i]])
        }
        print("APNs token retrieved: \(token)")
        
        // With swizzling disabled you must set the APNs token here.
        //InstanceID.instanceID().setAPNSToken(deviceToken, type: .prod)
        //InstanceID.instanceID().setAPNSToken(deviceToken, type: .sandbox)
        Messaging.messaging().apnsToken = deviceToken
        // FIRInstanceID.instanceID().setAPNSToken(deviceToken, type: FIRInstanceIDAPNSTokenType.sandbox)
    }
    
    // [START connect_on_active]
    func applicationDidBecomeActive(_ application: UIApplication) {
        connectToFcm(tokenHasBeenRefreshed: false)
        print("applicationDidBecomeActive")
        
        
        //self.window?.Web?.beginAppearanceTransition(true, animated: false)
        //self.window?.rootViewController?.endAppearanceTransition()
    }
    // [END connect_on_active]
    
    // [START disconnect_from_fcm]
    func applicationDidEnterBackground(_ application: UIApplication) {
        //FIRMessaging.messaging().disconnect()
        //print("Disconnected from FCM.")
        print("applicationDidEnterBackground")
    }
    // [END disconnect_from_fcm]
    
    // Firebase notification received
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter,  willPresent notification: UNNotification, withCompletionHandler   completionHandler: @escaping (_ options:   UNNotificationPresentationOptions) -> Void) {
        
        // custom code to handle push while app is in the foreground
        print("Handle push from foreground\(notification.request.content.userInfo)")
        
        // data
        let bodyData = notification.request.content.userInfo["body"] as! String
        let titleData = notification.request.content.userInfo["title"] as! String
        let idData = notification.request.content.userInfo["id"] as! String
   //     print("[Data] Title:\(titleData) + body:\(bodyData)+ id:\(idData)")
        
        // notification
        let dict = notification.request.content.userInfo["aps"] as! NSDictionary
        let d : [String : Any] = dict["alert"] as! [String : Any]
        let body : String = d["body"] as! String
        let title : String = d["title"] as! String
        print("[Notification] Title:\(title) + body:\(body)")

        
        if(showNotification) {
            //self.showAlertAppDelegate(title: title,message:body,buttonTitle:"ok",window:self.window!)
            self.showAlertAppDelegate(title: titleData,message:bodyData,buttonTitle:"ok",window:self.window!)
        }
    }
    
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        
        // if you set a member variable in didReceiveRemoteNotification, you  will know if this is from closed or background
        print("Handle push from background or closed\(response.notification.request.content.userInfo)")
        
        // data
        let bodyData = response.notification.request.content.userInfo["body"] as! String
        let titleData = response.notification.request.content.userInfo["title"] as! String
        let idData = response.notification.request.content.userInfo["id"] as! String
        print("[Data] Title:\(titleData) + body:\(bodyData) + id:\(idData)")
        
        // notification
        let dict = response.notification.request.content.userInfo["aps"] as! NSDictionary
        let d : [String : Any] = dict["alert"] as! [String : Any]
        let body : String = d["body"] as! String
        let title : String = d["title"] as! String
        print("[Notification] Title:\(title) + body:\(body)")
        
        //self.showAlertAppDelegate(title: title,message:body,buttonTitle:"ok",window:self.window!)
        //self.showAlertAppDelegate(title: titleData,message:bodyData,buttonTitle:"ok",window:self.window!)
        
        // save the news Identifier to open
        targetEventDetailsID = idData
        
        if(showNotification) {
            // open app
            let mainStoryboardIpad : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let targetVC : UIViewController = mainStoryboardIpad.instantiateViewController(withIdentifier: "WebViewController") as UIViewController
            self.window = UIWindow(frame: UIScreen.main.bounds)
            self.window?.rootViewController = targetVC
            self.window?.makeKeyAndVisible()
        }
    }
    
    func showAlertAppDelegate(title: String,message : String,buttonTitle: String,window: UIWindow){
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: buttonTitle, style: UIAlertActionStyle.default, handler: nil))
        window.rootViewController?.present(alert, animated: false, completion: nil)
    }
    
    // Firebase ended here
    
    // MARK: - Manage notification enabling
    
    func manageNotificayionEnabling(enabled: Bool) {
        showNotification = enabled
        if(!enabled){
            disconnectFromFcm()
        }
    }

    // MARK: - App Delegate Base App Methods

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        self.saveContext()
    }

    // MARK: - Core Data stack

    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
        */
        let container = NSPersistentContainer(name: "iVemmers")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                 
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    // MARK: - Core Data Saving support

    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
    
    // ------ SNIPPET PER ORIENTAMENTO APP (LANDSCAPE, PORTRAIT ---//
    
    var orientationLock = UIInterfaceOrientationMask.all
    func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask {
        return self.orientationLock
    }
    
    struct AppUtility {
        static func lockOrientation(_ orientation: UIInterfaceOrientationMask) {
            if let delegate = UIApplication.shared.delegate as? AppDelegate {
                delegate.orientationLock = orientation
            }
        }
        
        static func lockOrientation(_ orientation: UIInterfaceOrientationMask, andRotateTo rotateOrientation:UIInterfaceOrientation) {
            self.lockOrientation(orientation)
            UIDevice.current.setValue(rotateOrientation.rawValue, forKey: "orientation")
        }
    }
    
     // ------ END SNIPPET ---//
}
