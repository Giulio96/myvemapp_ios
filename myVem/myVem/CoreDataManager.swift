//
//  CoreDataManager.swift
//  myVem
//
//  Created by Federico Foschini on 23/03/17.
//  Copyright © 2017 myDev S.r.l. All rights reserved.
//

import UIKit
import Foundation
import CoreData

/*
 * Manager for application's Core Data
 */
class CoreDataManager {
    
    var appDel: AppDelegate!
    
    /*
     * Function to set the app delegate
     */
    func setAppDelegate(delegate: AppDelegate) {
        appDel = delegate
    }
    
    // MARK - User Data
    
    /*
     * Function to execute the auto login by loading the user logged core data
     */
    func loadUserLogged() -> User? {
        let moc = appDel.persistentContainer.viewContext
        let userFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "User")
        
        do {
            let users = try moc.fetch(userFetch) as! [User]
            if(users.count > 0) {
                //print("\(user.first!.token!) - \(user.first!.username!) - \(user.first!.password!)" )
                return users.first!
            }
            
        } catch {
            //fatalError("Failed to fetch person: \(error)")
            print("No user core data found")
        }
        return nil;
    }
    
    /*
     * Function to reset the core data about user profile
     */
    func resetUserData() {
        //print("Removing core data...")
        let moc = appDel.persistentContainer.viewContext
        let userFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "User")
        
        do {
            let users = try moc.fetch(userFetch) as! [User]
            if(users.count > 0) {
                for curUser in users {
                    moc.delete(curUser)
                }
            }
        } catch {
            print("No user core data found")
            //fatalError("Failed to fetch person: \(error)")
        }
        //print("Removed core data.")
    }
    
    /*
     * Function to save the core data about user profile
     */
    func saveUserIdData(userId: String) {
        
        let managedContext = appDel.persistentContainer.viewContext
        let entity = NSEntityDescription.entity(forEntityName: "User", in: managedContext)!
        let user = NSManagedObject(entity: entity, insertInto: managedContext)
        
        user.setValue(userId, forKeyPath: "userId")
        
        do {
            try managedContext.save()
        } catch let error as NSError {
            print("Could not save user id data. \(error), \(error.userInfo)")
        }
    }
    
    /*
     * Function to save the core data about user profile
     */
    func saveUserData(username: String, password: String, userId: String) {
        
        let managedContext = appDel.persistentContainer.viewContext
        let entity = NSEntityDescription.entity(forEntityName: "User", in: managedContext)!
        let user = NSManagedObject(entity: entity, insertInto: managedContext)
        
        user.setValue(username, forKeyPath: "username")
        user.setValue(password, forKeyPath: "password")
        user.setValue(userId, forKeyPath: "userId")
        
        do {
            try managedContext.save()
        } catch let error as NSError {
            print("Could not save user data. \(error), \(error.userInfo)")
        }
    }
    
    // MARK - Notification Token Data
    
    /*
     * Function to get the notification token core data
     */
    func loadNotificationTokenData() -> NotificationToken? {
        let moc = appDel.persistentContainer.viewContext
        let dataFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "NotificationToken")
        
        do {
            let tokens = try moc.fetch(dataFetch) as! [NotificationToken]
            if(tokens.count > 0) {
                return tokens.first!
            }
            
        } catch {
            //fatalError("Failed to fetch person: \(error)")
            print("No notification token core data found")
        }
        return nil;
    }
    
    /*
     * Function to reset the core data about session notiication token
     */
    func resetNotificationTokenData() {
        //print("Removing core data...")
        let moc = appDel.persistentContainer.viewContext
        let dataFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "NotificationToken")
        
        do {
            let tokens = try moc.fetch(dataFetch) as! [NotificationToken]
            if(tokens.count > 0) {
                for curToken in tokens {
                    moc.delete(curToken)
                }
            }
        } catch {
            print("No notiication token core data found")
            //fatalError("Failed to fetch person: \(error)")
        }
        //print("Removed core data.")
    }
    
    /*
     * Function to save the core data about session notiication token
     */
    func saveNotificationTokenData(token: String, requestResult: Bool) {
        let managedContext = appDel.persistentContainer.viewContext
        let entity = NSEntityDescription.entity(forEntityName: "NotificationToken", in: managedContext)!
        let notifToken = NSManagedObject(entity: entity, insertInto: managedContext)
        
        notifToken.setValue(token, forKeyPath: "token")
        notifToken.setValue(requestResult, forKeyPath: "requestResult")
        
        do {
            try managedContext.save()
        } catch let error as NSError {
            print("Could not save notification token data. \(error), \(error.userInfo)")
        }
        
        do{
            var userId = "0"//user.value(forKey: "userId") as? String""
            let userFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "User")
            
            do {
                let users = try managedContext.fetch(userFetch) as! [User]
                if(users.count > 0) {
                    let usr = users.first
                    userId = (usr?.userId)!
                }
            } catch {
                print("No user core data found")
                //fatalError("Failed to fetch person: \(error)")M
            }
            
            if(userId == "0")
            {
                return
            }
            
            var versionRelease = ""
            if let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String {
                versionRelease = version
            }
            print("softwareVersion\(versionRelease)")
            
            let systemVersion = UIDevice.current.systemVersion
            print("iOS\(systemVersion)")
            
            //iPhone or iPad
            let model = UIDevice.current.model
            print("device type=\(model)")
            
        
        //create the url with URL
        let url = URL(string: ((Configurations.UrlVerifyAccessDevice?.absoluteString)! + "?guid="+token+"&idUtente=\(userId)&os=2&secret="+Configurations.secretForAccessDevice+"&appVersion="+versionRelease+"&osVersion="+systemVersion+"&deviceType="+model)) //change the url
        
        if(url == nil)
        {
            return
        }
        
        //create the session object
        let session = URLSession.shared
        
        //now create the URLRequest object using the url object
        var request = URLRequest(url: url!)
        request.httpMethod = "POST" //set http method as POST
        
        
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        
        //create dataTask using the session object to send data to the server
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            
            guard error == nil else {
                return
            }
            
            guard let data = data else {
                return
            }
            
            do {
                print("salvato il FireBaseToken sul server!!")
            } catch let error {
                print(error.localizedDescription)
            }
        })
        task.resume()
        }
        catch{}
        
    }
}


