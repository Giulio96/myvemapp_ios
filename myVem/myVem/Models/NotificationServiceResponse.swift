//
//  NotificationServiceResponse.swift
//  myVem
//
//  Created by myDev on 4/27/17.
//  Copyright © 2017 myDev S.r.l. All rights reserved.
//

import Foundation
import JSONJoy

struct NotificationServiceResponse: JSONJoy {

    let success: Bool?
    var error: String?

    init(_ decoder: JSONLoader) throws {
        success = try decoder["Success"].get()
        error = nil
        
        //just an example of "checking" for a property.
        if let _: String = decoder["Error"].getOptional() {
            error = try decoder["Error"].get()
        }
    }
}
