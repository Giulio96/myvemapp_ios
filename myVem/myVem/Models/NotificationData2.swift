//
//  NotificationData2.swift
//  myVem
//
//  Created by myDev on 6/19/17.
//  Copyright © 2017 myDev S.r.l. All rights reserved.
//

import Foundation
import JSONJoy

struct NotificationData2: JSONJoy {
    let title: String?
    let body: String?
    
    init(_ decoder: JSONLoader) throws {
        title = try decoder["title"].get()
        body = try decoder["body"].get()
    }
}
