//
//  JavaScriptBridgeDelegate.swift
//  myVem
//
//  Created by Federico Foschini on 23/03/17.
//  Copyright © 2017 myDev S.r.l. All rights reserved.
//

import Foundation

/*
 * Protocol to manage JavaScriptBridge delegate functions and models
 */
protocol JavaScriptBridgeProtocol {
    
    /*
     * Function to log out
     */
    func logOut()
    
    /*
     * Function to log out
     */
    func openLogin()
    
    /*
     * Function to open a link
     */
    func openLink(linkToOpen: String)
    
    /*
     * Function to get FireBaseToken
     */
    func getFireBaseToken()->String
}
