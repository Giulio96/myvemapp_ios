//
//  NotificationData.swift
//  myVem
//
//  Created by myDev on 4/27/17.
//  Copyright © 2017 myDev S.r.l. All rights reserved.
//

import Foundation
import JSONJoy

struct NotificationData: JSONJoy {
    let title: String?
    let from: String?
    let body: String?
    let id: String?
    
    init(_ decoder: JSONLoader) throws {
        title = try decoder["Title"].get()
        from = try decoder["from"].get()
        body = try decoder["Body"].get()
        id = try decoder["Id"].get()
    }
}
