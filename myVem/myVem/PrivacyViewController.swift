//
//  PrivacyViewController.swift
//  myVem
//
//  Created by Vem Sistemi on 5/3/18.
//  Copyright © 2018 myDev S.r.l. All rights reserved.
//

import UIKit
import WebKit

class PrivacyViewController: UIViewController, WKNavigationDelegate, WKUIDelegate, UIScrollViewDelegate {

    @IBOutlet var containerView : UIView! = nil
    let coreDataManager = CoreDataManager()
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    weak var webView: WKWebView?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let webView = setupWebView()
        webView.autoresizingMask = UIViewAutoresizing.flexibleWidth; UIViewAutoresizing.flexibleHeight
        containerView.addSubview(webView)
        
        self.webView = webView
        //self.webView?.navigationDelegate = self
        
        self.webView?.navigationDelegate = self
        self.webView?.uiDelegate = self
        self.webView?.scrollView.delegate = self
        
        // setto la safe area, in questo modo correggo la banda bianca nella status bar di iphone x
        self.webView?.translatesAutoresizingMaskIntoConstraints = false
        if #available(iOS 11.0, *) {
            let guide = self.view.safeAreaLayoutGuide
            self.webView?.trailingAnchor.constraint(equalTo: guide.trailingAnchor).isActive = true
            self.webView?.leadingAnchor.constraint(equalTo: guide.leadingAnchor).isActive = true
            self.webView?.bottomAnchor.constraint(equalTo: guide.bottomAnchor).isActive = true
            self.webView?.topAnchor.constraint(equalTo: guide.topAnchor).isActive = true
        }
        //self.webView?.autoresizingMask = UIViewAutoresizing(rawValue: UIViewAutoresizing.RawValue(UInt8(UIViewAutoresizing.flexibleWidth.rawValue) | UInt8(UIViewAutoresizing.flexibleHeight.rawValue)))
        
        self.webView?.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        let langStr = Locale.current.languageCode
        let urlString = NSURL(string: (Configurations.UrlPrivacy?.absoluteString)! + "?lang=" + langStr!)
        
        self.webView?.load(URLRequest(url:urlString! as URL))
    }
    
    public func setupWebView() -> WKWebView{
        let userContentController = WKUserContentController()
        let webViewConfig = WKWebViewConfiguration()
        
        // Javascript that disables pinch-to-zoom by inserting the HTML viewport meta tag into <head>
        let source: String = "var meta = document.createElement('meta');" +
            "meta.name = 'viewport';" +
            "meta.content = 'width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no';" +
            "var head = document.getElementsByTagName('head')[0];" + "head.appendChild(meta);";
        let script: WKUserScript = WKUserScript(source: source, injectionTime: .atDocumentEnd, forMainFrameOnly: true)
        
        /* let preferences = WKPreferences()
         preferences.javaScriptEnabled = true
         preferences.javaScriptCanOpenWindowsAutomatically = true
         webViewConfiguration.preferences = preferences
         let contentController = WKUserContentController();
         contentController.add(
         jsBridge,
         name: "callbackHandler"
         )*/
        
    
        
        userContentController.addUserScript(script)
        
        webViewConfig.userContentController = userContentController
        webViewConfig.preferences.javaScriptCanOpenWindowsAutomatically = true
        //  let contentController = WKUserContentController();
        
        //webViewConfig.userContentController = contentController
        let frameWV = self.containerView.bounds
        let webViewFrame = CGRect(x: 0, y: 25, width: frameWV.size.width, height: frameWV.size.height - 25)
        
        return WKWebView(frame: webViewFrame, configuration: webViewConfig)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Hide the navigation bar on the this view controller
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    
}

extension String {
    
    
    var html2AttributedString: NSAttributedString? {
        guard
            let data = data(using: String.Encoding.utf8)
            else { return nil }
        do {
            return try NSAttributedString(data: data, options: [NSDocumentTypeDocumentAttribute:NSHTMLTextDocumentType,NSCharacterEncodingDocumentAttribute:String.Encoding.utf8], documentAttributes: nil)
        } catch let error as NSError {
            print(error.localizedDescription)
            return  nil
        }
    }
    var html2String: String {
        return html2AttributedString?.string ?? ""
    }
}
