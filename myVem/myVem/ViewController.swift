 //
//  ViewController.swift
//  myVem
//
//  Created by Federico Foschini on 22/03/17.
//  Copyright © 2017 myDev S.r.l. All rights reserved.
//

import UIKit
import SafariServices
import CoreData
import SwiftSpinner
import Firebase

class ViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var usernameField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var remeberSwitch: UISwitch!
    @IBOutlet weak var privacySwitch: UISwitch!
    @IBOutlet weak var btSignup: UIButton!
    @IBOutlet weak var lblForgot: UILabel!
    @IBOutlet weak var lblPrivacy: UILabel!
  //  @IBOutlet weak var gestGoToPrivacy: UIGestureRecognizer!
    var cookies: String = ""
    
    let connectivityManager = ConnectivityManager()
    let coreDataManager = CoreDataManager()
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    let userId = "0" as String
    
    // keyboard dimensions
    //var keyboardPortraitHeight = 55
    //var keyboardLandscapeHeight = 25
    
    /*
     * Login button click event action
     */
    @IBAction func loginClicked() {
        print("login clicked")

        // check for internet connectivity
        let connectionIsAvailable = connectivityManager.isInternetAvailable()
        if(!connectionIsAvailable) {
            let alertController = UIAlertController(title: "Error".localized, message: "ConnectionNotAvailable".localized, preferredStyle: .alert)
            
            let defaultAction = UIAlertAction(title: "OK", style: .default, handler: nil)
            alertController.addAction(defaultAction)
            
            self.present(alertController, animated: true, completion: nil)
            return
        }
        
        // connect to fcm
        appDelegate.connectToFcm(tokenHasBeenRefreshed: true)
        
        // get data from textfields
        let username = self.usernameField.text
        let password = self.passwordField.text
        let privacy = self.privacySwitch.isOn == false ? "false" : "true"
        print("username: \(String(describing: username))")
        //print("password: \(password)")
        
        // check remember login switch
        let rememberLoginData = self.remeberSwitch.isOn
        print("remember login data: \(rememberLoginData)")
        
        if((username?.isEmpty)! || (password?.isEmpty)!) {
            let alertController = UIAlertController(title: "Error".localized, message: "LoginError".localized, preferredStyle: .alert)
            
            let defaultAction = UIAlertAction(title: "OK", style: .default, handler: nil)
            alertController.addAction(defaultAction)
            
            self.present(alertController, animated: true, completion: nil)
            return
       //     UIAlertView(
         //       title: "Error".localized,
           //     message: "LoginError".localized,
           //     delegate: self,
            //    cancelButtonTitle: "OK")
             //   .show()
            
        } else {
            
            // La privacy deve essere flaggata
            if(privacy == "false") {
                let alertController = UIAlertController(title: "Error".localized, message: "Privacy".localized, preferredStyle: .alert)
                
                let defaultAction = UIAlertAction(title: "OK", style: .default, handler: nil)
                alertController.addAction(defaultAction)
                
                self.present(alertController, animated: true, completion: nil)
                return
               // UIAlertView(
                 //   title: "Error".localized,
                  //  message: "Privacy".localized,
                   // delegate: self,
                   // cancelButtonTitle: "OK")
                   // .show()
                //return
            }
            

            // In questo punto faccio l'autenticazione sul sito di Myvem
            // Lo faccio facendo una post sull'url /AppMyVem/Login?Username=22&Password=213
            //var url = Configurations.UrlLogin
            
            
            //declare parameter as a dictionary which contains string as key and value combination. considering inputs are valid
            let token = InstanceID.instanceID().token()!
            
            var versionRelease = ""
            if let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String {
                versionRelease = version
            }
            
            let parameters = ["Username": username, "Password": password, "Privacy": privacy, "FireBaseToken": token, "AppVersion": versionRelease, "JsonResponse" : "true"]
            
            //create the url with URL
            let url = URL(string: (Configurations.UrlLogin?.absoluteString)!) //change the url
            
            if(url == nil)
            {
                return
            }
            
            //create the session object
            let session = URLSession.shared
            
            //now create the URLRequest object using the url object
            var request = URLRequest(url: url!)
            request.httpMethod = "POST" //set http method as POST
            
            do {
                request.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted) // pass dictionary to nsdata object and set it as request body
            } catch let error {
                print(error.localizedDescription)
            }
            
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            request.addValue("application/json", forHTTPHeaderField: "Accept")
            
            //create dataTask using the session object to send data to the server
            let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
                
                guard error == nil else {
                    return
                }
                
                guard let data = data else {
                    return
                }
                
                do {
                    
                    if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode == 200{
                        print("statusCode should be 200, but is \(httpStatus.statusCode)")
                        print("response = \(String(describing: response))")
                        print("header = \(String(describing: httpStatus.allHeaderFields["Set-Cookie"]))")
                       // self.cookies = (httpStatus.allHeaderFields["Set-Cookie"] as? String)!
                        let cookiesExtract = HTTPCookie.cookies(withResponseHeaderFields: httpStatus.allHeaderFields as! [String : String], for: (response?.url!)!) as! [HTTPCookie]
                        
                        //HTTPCookieStorage.shared.setCookies(cookiesExtract as [AnyObject] as! [HTTPCookie], for: response?.url!, mainDocumentURL: nil)

                    }
                    
                    if let httpResponse = response as? HTTPURLResponse, let contentType = httpResponse.allHeaderFields["Set-Cookie"] as? String {
                        // use contentType here
                        print("header = \(String(describing: contentType))")
                    }
                    
                    //create json object from data
                    if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any] {
                        print(json)
                        if(json["Success"] as? String == "ok" && json["UserId"] as? String != "0")
                        {
                            let userId = json["UserId"] as? String;
                            
                         //   HTTPCookieStorage.shared.cookieAcceptPolicy = HTTPCookie.AcceptPolicy.always
                            // Salvo l'user id nello store
                       //    self.coreDataManager.saveUserIdData(userId: userId!)
                            
                            OperationQueue.main.addOperation {
                               self.performSegue(withIdentifier: "OpenWebViewSegue", sender: self)
                            }
                            
                            print("successo")
                            // l'applicazione si ricorda sempre i dati
                           // if(rememberLoginData) {
                                // save user core data
                            self.coreDataManager.saveUserData(username: username!, password: password!, userId: userId!)
                                let token = InstanceID.instanceID().token()
                                if(token != nil){
                                    self.coreDataManager.saveNotificationTokenData(token: token!, requestResult: true)
                                }
                                
                                print("user data saved")
                           // }
                           
                        }
                        else{
                             OperationQueue.main.addOperation {
                                let alertController = UIAlertController(title: "Error".localized, message: "LoginError".localized, preferredStyle: .alert)
                                
                                let defaultAction = UIAlertAction(title: "OK", style: .default, handler: nil)
                                alertController.addAction(defaultAction)
                                
                                self.present(alertController, animated: true, completion: nil)
                                return
                          //  UIAlertView(
                           //     title: "Error".localized,
                             //   message: "LoginError".localized,
                              //  delegate: self,
                               // cancelButtonTitle: "OK")
                                //.show()
                            //return
                            }
                        }
                        
                        // handle json...
                    }
                } catch let error {
                    print(error.localizedDescription)
                }
            })
            task.resume()
            
        }
    }
    
    /*
     Funzione che permette di settare i cookie
     */
    func setCookie(key: String, value: AnyObject) {
        let cookieProps: [HTTPCookiePropertyKey : Any] = [
            HTTPCookiePropertyKey.domain: Configurations.WebAppBaseUrl,
            HTTPCookiePropertyKey.path: "/",
            HTTPCookiePropertyKey.name: key,
            HTTPCookiePropertyKey.value: value,
            HTTPCookiePropertyKey.secure: "TRUE",
            HTTPCookiePropertyKey.expires: NSDate(timeIntervalSinceNow: 10000000000)
        ]
        
        if let cookie = HTTPCookie(properties: cookieProps) {
            HTTPCookieStorage.shared.setCookie(cookie)
        }
    }

    
    /*
     * Signup button click event action
     */
    @IBAction func signupClicked() {
        print("signup clicked")
        let signupUrl = Configurations.UrlSignUp
        print("open link: [\(signupUrl)]")
        //UIApplication.shared.openURL(signupUrl! as URL)
        
        // open the SafariViewController to sign up
        let safariVC = SFSafariViewController(url: signupUrl! as URL)
        self.present(safariVC, animated: true, completion: nil)
    }
    
    /*
     * Prepare for segue
     */
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "OpenWebViewSegue" {
            let controller = segue.destination as! WebViewController
            controller.userName = self.usernameField.text!
            controller.userPassword = self.passwordField.text!
            controller.cookies = self.cookies
        }
    
    }
    
    /*override var shouldAutorotate: Bool {
        return false
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }*/
    
    override func viewDidLoad() {
        print("View did load")
        
        super.viewDidLoad()
    
        
        // reset application badge number
        UIApplication.shared.applicationIconBadgeNumber = 0
        
        // delegate for keyboard textfields
        self.usernameField.delegate = self;
        self.passwordField.delegate = self;
        //self.usernameField.inputAccessoryView = nil
        //self.passwordField.inputAccessoryView = nil
        if #available(iOS 10, *) {
            // Disables the password autoFill accessory view.
            self.usernameField.textContentType = UITextContentType("")
            self.passwordField.textContentType = UITextContentType("")
        }
        
        self.lblForgot.text = "LblForgot".localized;
        
        self.usernameField.setBottomBorder()
        self.passwordField.setBottomBorder()
        
        // Aggiungo la tap gesture alla label sulla privacy
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.tapFunction))
        self.lblPrivacy.isUserInteractionEnabled = true
        self.lblPrivacy.addGestureRecognizer(tap)
        
        // TEst
       // self.usernameField.text = "provaTreossi7"
       // self.passwordField.text = "provaTreossi7"
        
        // Sottolineo la parola privacy policy in inglese e privacy in italiano
        let string              = self.lblPrivacy.text
        var range               = (string! as NSString).range(of: "privacy policy")
        if range.length <= 0{
            range = (string! as NSString).range(of: "informativa privacy")
        }
        
        let attributedString    = NSMutableAttributedString(string: string!)
        attributedString.addAttribute(NSUnderlineStyleAttributeName, value: NSNumber(value: 1), range: range)
        
        self.lblPrivacy.attributedText = attributedString
        
        // setup components
        coreDataManager.setAppDelegate(delegate: appDelegate)
        
        // notifications for change oriention
        NotificationCenter.default.addObserver(self, selector: #selector(ViewController.rotated), name: NSNotification.Name.UIDeviceOrientationDidChange, object: nil)
        
        // notifications for keyboard
        NotificationCenter.default.addObserver(self, selector: #selector(ViewController.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(ViewController.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    func tapFunction(sender:UITapGestureRecognizer) {
        self.performSegue(withIdentifier: "OpenPrivacySegue", sender: self)
    }
    
    // { Signup Management
    
    /*
     * Manage the signup button and the related visibility
     */
    func checkSignup() {
        print("Checking for signup...")
        var isActive = false
        let myUrl = Configurations.UrlCheckSignUp
        //let myUrl = Configurations.iVememrsSignupIsActiveUrl
        
        // Creaste URL Request
        var request = URLRequest(url:myUrl! as URL);
        
        // Set request HTTP method to GET. It could be POST as well
        request.httpMethod = "GET"
        
        // If needed you could add Authorization header value
        // Add Basic Authorization
        /*
         let username = "myUserName"
         let password = "myPassword"
         let loginString = NSString(format: "%@:%@", username, password)
         let loginData: NSData = loginString.dataUsingEncoding(NSUTF8StringEncoding)!
         let base64LoginString = loginData.base64EncodedStringWithOptions(NSDataBase64EncodingOptions())
         request.setValue(base64LoginString, forHTTPHeaderField: "Authorization")
         */
        
        // Or it could be a single Authorization Token value
        //request.addValue("Token token=884288bae150b9f2f68d8dc3a932071d", forHTTPHeaderField: "Authorization")
        
        // Excute HTTP Request
        let task = URLSession.shared.dataTask(with: request) {
            data, response, error in
            
            // Check for error
            if error != nil
            {
                self.btSignup.isHidden = true
                print("error=\(String(describing: error))")
                return
            }
            
            // Print out response string
            let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
            print("responseString = \(String(describing: responseString))")
            
            // Convert server json response to NSDictionary
            do {
                if let convertedJsonIntoDict = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                    
                    // Print out dictionary
                    print(convertedJsonIntoDict)
                    
                    // Get value by key
                    isActive = (convertedJsonIntoDict["active"] as? Bool)!
                    print("Is Signup Active: \(isActive)")
                    self.manageSignupButtonVisibility(isVisibile: !isActive)
                //    print("Signup button visibile: \(!self.btSignup.isHidden)")
                }
                
            } catch let error as NSError {
                print(error.localizedDescription)
            }
            self.manageSignupButtonVisibility(isVisibile: !isActive)
        }
        task.resume()
    }
    
    /*
     * Manage the signup button visibility
     */
    func manageSignupButtonVisibility(isVisibile: Bool) {
        DispatchQueue.main.async {

                if(self.btSignup != nil)
                {
                        self.btSignup.isHidden = isVisibile
                }
           
            SwiftSpinner.hide()
        }
    }
    
    // }
    
    // { Rotation Management
    
    /*
     * Manage the rotation
     */
    func rotated() {
        if UIDeviceOrientationIsLandscape(UIDevice.current.orientation) {
            print("Rotated -> Landscape")
        }
        if UIDeviceOrientationIsPortrait(UIDevice.current.orientation) {
            print("Rotated -> Portrait")
        }
        //self.view.endEditing(true)
        // self.view.frame.origin.y = 0
    }
    
    // }
    
    // { Keyboard Management
    
    /*
     * Manage the keyboard
     */
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    
    
    /*
     * Manage the keyboard show
     */
    func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            self.view.layoutIfNeeded()
            if self.view.frame.origin.y == 0{
                self.view.frame.origin.y -= keyboardSize.height
            }
        }
    }
    
    /*
     * Manage the keyboard hide
     */
    func keyboardWillHide(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y != 0{
               // self.view.frame.origin.y += keyboardSize.height
             //   if(self.view.frame.origin.y<0)
               // {
                    self.view.frame.origin.y = 0;
               // }
            }
        }
    }
    
    // }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Hide the navigation bar on the this view controller
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
      
        AppDelegate.AppUtility.lockOrientation(.portrait)

        // show spinner
        SwiftSpinner.show("Configuring".localized)
        //SwiftSpinner.show(duration: 7.0, title: "Configuring".localized)

        // check if it's possible to signup
        checkSignup()
        
        // check user data saved
        let loggedUser = coreDataManager.loadUserLogged()
        if loggedUser != nil {
            
            if(loggedUser?.username == nil || loggedUser?.password == nil)
            {
                print("User logged but without user or pass")
                return
            }
            
                    print("Logged user found: \(String(describing: loggedUser?.username))")
            // auto login
            usernameField.text = loggedUser?.username
            passwordField.text = loggedUser?.password
            
            // check for internet connectivity
            let connectionIsAvailable = connectivityManager.isInternetAvailable()
            if(!connectionIsAvailable) {
                let alertController = UIAlertController(title: "Error".localized, message: "ConnectionNotAvailable".localized, preferredStyle: .alert)
                
                let defaultAction = UIAlertAction(title: "OK", style: .default, handler: nil)
                alertController.addAction(defaultAction)
                
                present(alertController, animated: true, completion: nil)
                return
            }
            
            self.performSegue(withIdentifier: "OpenWebViewSegue", sender: self)
            
        } else {
            print("Logged user not found")
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // setto il colore del testo nella status bar in bianco
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
}
 
 extension UITextField {
    func setBottomBorder() {
        let border = CALayer()
        let width = CGFloat(1.0)
        border.borderColor = UIColor.white.cgColor
        border.frame = CGRect(x: 0, y: self.frame.size.height - width, width:  self.frame.size.width, height: self.frame.size.height)
        
        border.borderWidth = width
        self.layer.addSublayer(border)
        self.layer.masksToBounds = true
    }
 }
 
 extension UIColor {
    
    convenience init(rgbColorCodeRed red: Int, green: Int, blue: Int, alpha: CGFloat) {
        
        let redPart: CGFloat = CGFloat(red) / 255
        let greenPart: CGFloat = CGFloat(green) / 255
        let bluePart: CGFloat = CGFloat(blue) / 255
        
        self.init(red: redPart, green: greenPart, blue: bluePart, alpha: alpha)
        
    }
 }

