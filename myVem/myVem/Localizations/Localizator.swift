//  Localizator.swift
//  based on medium post :
//  https://medium.com/@dcordero/a-different-way-to-deal-with-localized-strings-in-swift-3ea0da4cd143#.mldz1bh9y
//  swift 2 version writting by "David Cordero"
//  update to swift 3 attempt by "ba_bessem"
//  Created on 06/01/17.
//  Copyright © Pirana. All rights reserved.
//
// (Dictionary with subdict)
// User :
//      NAME :
//              VALUE: name
//              COMMENT:  user name
//      ATTRIBUTE :
//              VALUE: another attribute
//              COMMENT:  user extras
//
// to print the localized variable of User NAME
// just : print("User NAME".localized)
//
//
// (Dictionary with string values as sub)
//      NAME :
//              VALUE: pirana
//              COMMENT:  user name
// to print the localized variable of NAME
// just : print("NAME".localized)
//

import Foundation

class Localizator {
    
    static let sharedInstance = Localizator()
    
    lazy var localizableDictionary: [String:Any] = {
        return localizableAsDictionary(forResource:"Localizable", withExtension: "plist")
    }()
    
    func localize(string: String) -> String {
        if(split(splittable: string).count == 1){
            guard let localizedString = (localizableDictionary[string] as AnyObject)["value"] as? String else {
                assertionFailure("Missing translation for: \(string)")
                return string
            }
            return localizedString
        }else{
            return parseLocalizedModel(path : string)
        }
    }
    
    fileprivate func parseLocalizedModel(path : String) -> String{
        let pathAsTable = split(splittable: path) as [String]
        let depth = pathAsTable.count
        var model = [String :Any]()
        guard let localizedModel = (localizableDictionary[pathAsTable.first!] as AnyObject)[pathAsTable[1]] as? [String : Any] else {
            assertionFailure("error translation for: \(path)")
            return path
        }
        var index = 2
        model = localizedModel
        while (index < depth) {
            index += 1
            model = localizedModel[pathAsTable[index]] as! [String : Any]
        }
        
        guard let localizedString = (model["value"] as? String) else {
            assertionFailure("Missing translation for: \(path)")
            return path
        }
        return localizedString
        
    }
    
    fileprivate func split(splittable : String) -> [String]{
        return splittable.components(separatedBy: " ")
    }
}

func localizableAsDictionary(forResource: String, withExtension: String) -> [String : Any] {
    var dict = [String : Any]()
    do {
        if let url = Bundle.main.url(forResource:"Localizable", withExtension: "plist") {
            // Fix l'url in caso di lingua impostata in italiano
            let langStr = Locale.current.languageCode
            
            var urlNS = url
            let absUrl = url.absoluteString
            if absUrl.range(of:"/Base.lproj") != nil && langStr == "it" {
                urlNS = NSURL(string:absUrl.replacingOccurrences(of: "Base", with: "it"))! as URL
            }
            let data = try Data(contentsOf:urlNS)
            let swiftDictionary = try PropertyListSerialization.propertyList(from: data, options: [], format: nil) as! [String:Any]
            dict = swiftDictionary
        }
    }catch let error {
        print(error)
    }
    return dict
}

extension String {
    var localized: String {
        return Localizator.sharedInstance.localize(string: self)
    }
}

