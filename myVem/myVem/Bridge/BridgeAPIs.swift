//
//  BridgeAPIs.swift
//  myVem
//
//  Created by Federico Foschini on 23/03/17.
//  Copyright © 2017 myDev S.r.l. All rights reserved.
//

import Foundation

/*
 * JavaScript Bridge's APIs
 */
class BridgeAPIs {
    
    /*
     * API to logout
     */
    public static let LogOutApi = "logOut"
    
    /*
     * API to openLogin
     */
    public static let OpenLogin = "openLogin"
    
    /*
     * API to open a link
     */
    public static let OpenLinkApi = "openLink"
    
    /*
     * API to get firebase token
     */
    public static let GetFireBaseToken = "getFireBaseToken"

}
