//
//  JavaScriptBridge.swift
//  myVem
//
//  Created by Federico Foschini on 22/03/17.
//  Copyright © 2017 myDev S.r.l. All rights reserved.
//

import Foundation
import WebKit
import CoreLocation
import CoreBluetooth

/*
 * JavaScript Bridge
 */
class JavaScriptBridge : NSObject, WKNavigationDelegate, WKScriptMessageHandler {
    
    var webView: WKWebView!
    var jsProtocol: JavaScriptBridgeProtocol!
    
    /*
     * Function to set the web view
     */
    func setBridgeWebView(view: WKWebView) {
        webView = view
    }
    
    /*
     * Function to set the JavaScript bridge protocol
     */
    func setProtocol(targetProtocol: JavaScriptBridgeProtocol) {
        jsProtocol = targetProtocol
    }
    
    /*
     * Function to manage the user content controller's messages
     */
    func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
        let msgContent = message.body as! String
        print(msgContent)
        
        var values = msgContent.components(separatedBy: "|")
        switch values[0] {
            
            case BridgeAPIs.LogOutApi:
                print("LogOut API called")
                jsProtocol.logOut()
                break
            
            case BridgeAPIs.OpenLinkApi:
                print("OpenLink API called")
                jsProtocol.openLink(linkToOpen: values[1])
                break
            
            case BridgeAPIs.OpenLogin:
                print("OpenLogin API called")
                jsProtocol.openLogin()
                break
            
            case BridgeAPIs.GetFireBaseToken:
                print("GetFireBaseToken API called")
                let token = jsProtocol.getFireBaseToken()
                var versionRelease = ""
                if let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String {
                    versionRelease = version
                }
                callJavaScriptFunction(jsFunction: "MobileBridge.OnTokenRetrieved", parameters: "\"\(token)\",\"\(versionRelease)\"")
                break
            
            default:
                print("API [\(values[0])] for message [\(msgContent)] not recognized.")
        }
    }
    
    /*
     * Function to call a JavaScript function from the device
     */
    func callJavaScriptFunction(jsFunction: String, parameters: String) {
      //  let js = "javascript:DeviceBridge.\(jsFunction)(\(parameters))"
        let js = "javascript:\(jsFunction)(\(parameters))"
        print(js)
        webView!.evaluateJavaScript(js, completionHandler: nil)
    }
}

